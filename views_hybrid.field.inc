<?php
/**
 * @file
 * Contains field processing stuff for Views Hybrid.
 */

/**
 * Implements hook_field_formatter_prepare_view().
 */
function views_hybrid_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items, $displays) {
  $nids = array();
  $count_handpicked_items = 0;
  foreach ($items as $nid => $item) {
    // Include the parent node so we can filter it out of the view.
    $nids[] = $current_nid = $nid;
    // Get the settings that the user specified for the field formatter.
    $settings = $displays[$nid]['settings'];
    // Get a count of the hand-picked items to subtract from the view total.
    $count_handpicked_items = count($item);
    // Get a list of the hand-picked nodes, to exclude from the view.
    foreach ($item as $ref) {
      $nids[] = $ref['target_id'];
    }
  }
  // Get the selected number of items from the selected view and display.
  $display = $settings['display'];
  $count = $settings['items_count'] - $count_handpicked_items;
  $view = views_hybrid_get_view_object($settings['view'], $display, $count, $nids);
  if ($view) {
    // There is probably a more robust way to handle arguments than this.
    $node = $entities[$current_nid];
    $arguments = explode(',', $settings['arguments']);
    foreach ($arguments as $argument) {
      $arg_vals[] = views_hybrid_parse_args($argument, $node);
    }
    $view->set_arguments($arg_vals);

    // We're finally ready to execute the query. Hooray!
    $view->pre_execute();
    $view->execute();
    $response = $view->result;

    // Gather up the response nids and send them off to be rendered
    foreach ($response as $node) {
      // Add it to the entityreference array.
      $items[$current_nid][] = array(
        'target_id' => $node->nid,
      );
    }
  }
  else {
    drupal_set_message(t('Could not fetch view "@view" with display "@display". Check that both view and display exist.', array('@view' => $settings['view'], '@display' => $display)), WATCHDOG_WARNING, FALSE);
  }
}

/**
 * Implements hook_field_formatter_view().
 */
function views_hybrid_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  // Get the target items.
  $nids = array();
  foreach ($items as $item) {
    $nids[] = $item['target_id'];
  }
  $nodes = node_load_multiple($nids);

  // Display them according to the user's choice of view mode.
  $view_mode = $display['settings']['view_mode'];

  // We have our own special fake view mode for simple linked titles.
  if ($view_mode == 'linked_title_only') {
    $element = array(
      '#items' => array(),
      '#theme' => 'item_list',
      '#attributes' => array(
        'class' => array('views-hybrid-list'),
      ),
    );
    // Divide the space-delimited string of classes.
    $classes = !empty($display['settings']['classes']) ? preg_split('/[\s]+/', $display['settings']['classes']) : array();
    foreach ($nodes as $node) {
      $element['#items'][] = array(
        'data' => l($node->title, "node/$node->nid", array('attributes' => array('class' => $classes))),
        'class' => array('views-hybrid-item'),
      );
    }
    return $element;
  }
  else {
    $entities = entity_view('node', $nodes, $view_mode);
    return $entities['node'];
  }
}

/**
 *  Gets the view object.
 *
 * @param string $view_name
 *  The machine name of the view to use.
 * @param string $display_name
 *  The machine name of the views display to use.
 * @param int $count
 *  The number of items to retrieve.
 * @param array $exclude_nids
 *  The list of nids to filter out.
 * @return array
 *   The list of node objects.
 */
function views_hybrid_get_view_object($view_name, $display_name, $count = NULL, $exclude_nids = array()) {
  $view = views_get_view($view_name);
  if ($view) {
    if (in_array($display_name, array_keys($view->display))) {
      $view->set_display($display_name);

      // Make sure we get the right number of results.
      if (!empty($count)) {
        $view->set_items_per_page($count);
        $view->display[$display_name]->display_options['pager']['type'] = 'some';
      }

      // Filter out the current node, as well as the handpicked nodes.
      foreach ($exclude_nids as $nid) {
        $options = array(
          'operator' => '!=',
          'value' => array('value' => $nid),
        );
        $view->add_item($display_name, 'filter', 'node', 'nid', $options);
      }
      return $view;
    }
    else {
      watchdog('views_hybrid', 'View %view does not have a display called %display. ', array('%view' => $view_name, '%display' => $display_name), WATCHDOG_WARNING);
    }
  }
  else {
    watchdog('views_hybrid', 'View %view could not be loaded', array('%view' => $view_name), WATCHDOG_WARNING);
  }
}

/**
 * Takes a string that represents an argument for a View, returns the value of
 * that argument for the node passed in.
 *
 * @param string $argument
 *  The name of the argument. Could be a field name or a node attribute.
 * @param obj $argument
 *  The node to get the argument values from.
 * @return array
 *  An array of the values that correspond to the argument asked of the node.
 */
function views_hybrid_parse_args($argument, $node) {
  $argument = trim($argument);
  // Make sure we really have an argument.
  if (!empty($argument)) {
    // Fields and such will be arrays. Parse them.
    if (is_array($node->{$argument})) {
      if (strpos($argument, 'field_') === 0) {
        // Congratulations! It's a field.
        $field_data = views_hybrid_parse_field_args($argument, $node);
        // @todo: Add handling for fields with multiple columns.
        return isset($field_data[0]) ? $field_data[0] : NULL;
      }
    }
    // Simple entity properties like type, nid, status, etc. are strings.
    else {
      return $node->{$argument};
    }
  }
}

/**
 * Pulls values out of fields and turns them into multivalue Views arguments.
 *
 * @param string $field_name
 *  The name of the field to get values from.
 * @param obj $node
 *  The node object to get values from.
 * @return array
 *  Returns an array of imploded arrays of values, suitable for Views arguments.
 */
function views_hybrid_parse_field_args($field_name, $node) {
    $field_info = field_info_field($field_name);
    $values = array();
    foreach (array_keys($field_info['columns']) as $column_name) {
      if($field_values = _field_get_column_values('node', $node, $field_name, $column_name)) {
        $values[] = implode(',', $field_values);
      }
    }
    return $values;
}

/**
 * Helper function for getting values out of fields.
 * Taken directly from http://drupal.stackexchange.com/questions/38813/easiest-way-to-get-term-entities-from-a-term-reference-field
 * Thanks, Letharion!
 * @param string $entity_type
 * @param obj $entity
 * @param string $field_name
 * @param string $column_name
 * @return array or boolean
 *  Returns an array of values or FALSE if none exist.
 */
function _field_get_column_values($entity_type, $entity, $field_name, $column_name) {
  if ($field_items = field_get_items($entity_type, $entity, $field_name)) {
    $values = array();
    foreach ($field_items as $item) {
      if (isset($item[$column_name])) {
        $values[] = $item[$column_name];
      }
    }
    return empty($values) ? FALSE : $values;
  }

  return FALSE;
}

