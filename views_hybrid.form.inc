<?php
/**
 * @file
 * Contains field formatter options and form definitions for Views Hybrid.
 */


/**
 * Implements hook_field_formatter_info().
 */
function views_hybrid_field_formatter_info() {
  return array(
    'views_hybrid' => array(
      'label' => t('Views Hybrid'),
      'field types' => array('entityreference'),
      'settings' => array(
        'items_count' => '5',
        'view' => 'frontpage',
        'display' => 'linked_title_only',
        'classes' => array(),
        'view_mode' => 'teaser',
        'arguments' => '',
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function views_hybrid_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {  $display = $instance['display'][$view_mode];

  // Create a list of all view modes by ID and label for users to select from.
  $entity_info = entity_get_info('node');
  $modes = array();
  foreach ($entity_info['view modes'] as $mode_id => $mode_info) {
    $modes[$mode_id] = $mode_info['label'];
  }
  $options = $modes;
  $options['linked_title_only'] = 'Linked title only';

  // Create the settings form.
  $settings = $display['settings'];
  $element = array();
  $element['items_count'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Number of items to display'),
    '#description'    => t('Large numbers may cause performance issues.'),
    '#default_value'  => $settings['items_count'],
    );
  $element['view'] = array(
    '#type'           => 'textfield',
    '#title'          => t('View to pull from'),
    '#description'    => t('Select the view that will provide the node IDs of the filler items.'),
    '#default_value'  => $settings['view'],
  );
  $element['display'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Display to pull from'),
    '#description'    => t('Select the display of the view that will provide the node IDs of the filler items.'),
    '#default_value'  => $settings['display'],
  );
  $element['view_mode'] = array(
    '#type'           => 'select',
    '#options'        => $options,
    '#title'          => t('View mode for displaying entities'),
    '#description'    => t('Select the view mode for all the items in the field.'),
    '#default_value'  => $settings['view_mode'],
  );
  $element['classes'] = array(
    '#type'           => 'textfield',
    '#title'          => t('CSS classes'),
    '#description'    => t('CSS classes to add to the links, if "Linked title only" was chosen as the view mode. These add to the &lt;a> elements. The &lt;li> elements\'s classes can be overridden with theme functions.'),
    '#default_value'  => $settings['classes'],
    '#states'         => array(
      'visible' => array(
        // This is how it shows up on the core node display settings form.
        ':input[name="fields[field_related_content][settings_edit_form][settings][view_mode]"]' => array('value' => 'linked_title_only'),
      ),
    ),
  );
  $element['arguments'] = array(
    '#type'           => 'textfield',
    '#options'        => $arguments,
    '#title'          => t('Views argument(s)'),
    '#description'    => t('If your view takes arguments, enter the field\'s machine names here. <br /> If there\'s more than one, separate with commas and enter them in the order that views uses them. <br /> Example: "field_foo,field_bar,field_baz"'),
    '#default_value'  => $settings['arguments'],
  );
  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function views_hybrid_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $summary = t('Fill field to @size of items (rendered as @mode), from view: @view:@display (arguments: @arguments)', array(
    '@size'     => $settings['items_count'],
    '@view'  => $settings['view'],
    '@display'  => $settings['display'],
    '@mode'  => $settings['view_mode'],
    '@arguments'  => $settings['arguments']?:'none',
  ));
  return $summary;
}
